#include "transitions.h"

using namespace std;

// These string constants are taken from Prost's parser output, and should NOT be touched
const string Dependency::sdet = "## number of det state fluents";
const string Dependency::sprob = "## number of prob state fluents";
const string Dependency::sactions = "## number of action fluents";
const string Dependency::astart = "#####ACTION FLUENTS#####";
const string Dependency::dstart = "#####DET STATE FLUENTS AND CPFS#####";
const string Dependency::pstart = "#####PROB STATE FLUENTS AND CPFS#####";
const string Dependency::sname = "## name";
const string Dependency::sindex = "## index";
const string Dependency::sformula = "## formula";
const string Dependency::sreward = "#####REWARD#####";


// Dependency::Dependency(const char* in, const char* out, const char* ws, const char* aws) : nsfluents(0), nafluents(0) {
Dependency::Dependency(string in, string out) : nsfluents(0), nafluents(0) {
	infile = in;
	outfile1 = out;
	// outfile2 = ws;
	// outfile3 = aws;
}


void Dependency::process() {
	string line;

	ifstream ifile(infile.c_str());
	while (getline(ifile, line)) {
		if (line == sdet) {
			getline(ifile, line);
			nsfluents += atof(line.c_str());
		}
		if (line == sprob) {
			getline(ifile, line);
			nsfluents += atof(line.c_str());
		}
		if (line == sactions) {
			getline(ifile, line);
			nafluents += atof(line.c_str());
			nafluents++;
		}
		if (line == astart) break;
	}

	sfluents = new set<int>[nsfluents];
	afluents = new set<int>[nafluents];
	sfargs = new vector<string>[nsfluents];
	afargs = new vector<string>[nafluents];
	sfwts = new vector<string>[nsfluents];
	afwts = new vector<string>[nafluents];

	int aindex = 0;
	set<string> daf; // distinct action fluent names
	afnames[aindex] = "noop";
	daf.insert("noop");
	stringstream ssa;
	while (getline(ifile, line)) {
		if (line == sindex) {
			getline(ifile, line);
			aindex++;
		}
		else if (line == sname) {
			getline(ifile, line);
			int p = line.find("(");
			string temp = line.substr(0, p);

			int q = line.find(")");
			string args = line.substr(p+1, q-p-1);
			// cout << line << endl << temp << endl << args << endl;
			for (int u = 0; u < args.length(); u++) {
				if (args[u] == ',' || args[u] == ' ') {
					if ((ssa.str()).length() > 0) {
						afargs[aindex].push_back(ssa.str());
						ssa.str(std::string());
					}
				}
				else {
					ssa << args[u];
				}
			}
			afargs[aindex].push_back(ssa.str());
			ssa.str(std::string());
			//:
			afnames[aindex] = temp;
			daf.insert(temp);
		}
		if ((line == dstart) || (line == pstart)) break;
	}

	int index = -1;
	set<string> dsf; // distinct state fluent names
	stringstream ssc;
	while (getline(ifile, line)) {
		if (line == sindex) {
			getline(ifile, line);
			index++;
		}
		else if (line == sname) {
			getline(ifile, line);
			int p = line.find("(");
			string temp = line.substr(0, p);
			int q = line.find(")");
			string args = line.substr(p+1, q-p-1);
			// cout << line << endl << temp << endl << args << endl;
			for (int u = 0; u < args.length(); u++) {
				if (args[u] == ',' || args[u] == ' ') {
					if ((ssc.str()).length() > 0) {
						sfargs[index].push_back(ssc.str());
						ssc.str(std::string());
					}
				}
				else {
					ssc << args[u];
				}
			}
			sfargs[index].push_back(ssc.str());
			ssc.str(std::string());
			sfnames[index] = temp;
			dsf.insert(temp);
		}
		else if (line == sformula) {
			getline(ifile, line);
			int p1, p2;
			p1 = line.find("$s(");
			sfluents[index].insert(index); // fix
			while (p1 != string::npos) {
				p2 = line.find(")", p1);
				int svar = atof(line.substr(p1+3, p2-p1-3).c_str());
				sfluents[index].insert(svar);
				p1 = line.find("$s(", p2);
			}
			p1 = line.find("$a(");
			while (p1 != string::npos) {
				p2 = line.find(")", p1);
				int avar = atof(line.substr(p1+3, p2-p1-3).c_str());
				avar++;
				afluents[avar].insert(index);
				p1 = line.find("$a(", p2);
			}
		}
		else if (line == sreward) break;
	}

	int idx = 0;
	for (set<string>::const_iterator it = dsf.begin(); it != dsf.end(); it++) {
		dsfindex[(*it)] = idx;
		idx++;
	}

	idx = 0;
	for (set<string>::const_iterator it = daf.begin(); it != daf.end(); it++) {
		dafindex[(*it)] = idx;
		idx++;
	}

	ifile.close();
}


void Dependency::getStateFluentWeights() {
	set<string> dwt;
	for (int i = 0; i < nsfluents; i++) {
		for (set<int>::const_iterator it = sfluents[i].begin(); it != sfluents[i].end(); it++) {
			int j = (*it);
			stringstream ss;
			ss << dsfindex[sfnames[i]] << "," << dsfindex[sfnames[j]] << ",";
			for (vector<string>::const_iterator it1 = sfargs[i].begin(); it1 != sfargs[i].end(); it1++) {
				for (vector<string>::const_iterator it2 = sfargs[j].begin(); it2 != sfargs[j].end(); it2++) {
					if ((*it1) == (*it2)) {
						ss << "1,";
					}
					else {
						ss << "0,";
					}
				}
			}
			string sstr = ss.str();
			sstr.pop_back();
			sfwts[i].push_back(sstr);
			dwt.insert(sstr);
			ss.str(std::string());
		}
		sfwts[i].push_back(sfnames[i]);
	}

	int idx = 0;
	for (set<string>::const_iterator it = dwt.begin(); it != dwt.end(); it++) {
		swtindex[(*it)] = idx;
		idx++;
	}

	// for bias weight
	for (map<string, int>::const_iterator it = dsfindex.begin(); it != dsfindex.end(); it++) {
		swtindex[it->first] = idx;
		idx++;
	}
}



void Dependency::getActionFluentWeights() {
	set<string> dwt;
	for (int i = 0; i < nafluents; i++) {
		for (int j = 0; j < nsfluents; j++) {
			stringstream ss;
			ss << dafindex[afnames[i]] << "," << dsfindex[sfnames[j]] << ",";
			for (vector<string>::const_iterator it1 = afargs[i].begin(); it1 != afargs[i].end(); it1++) {
				for (vector<string>::const_iterator it2 = sfargs[j].begin(); it2 != sfargs[j].end(); it2++) {
					if ((*it1) == (*it2)) {
						ss << "1,";
					}
					else {
						ss << "0,";
					}
				}
			}
			string sstr = ss.str();
			sstr.pop_back();
			afwts[i].push_back(sstr);
			dwt.insert(sstr);
			ss.str(std::string());
		}
		afwts[i].push_back(afnames[i]);
	}

	int idx = 0;
	for (set<string>::const_iterator it = dwt.begin(); it != dwt.end(); it++) {
		awtindex[(*it)] = idx;
		idx++;
	}

	// for bias weight
	for (map<string, int>::const_iterator it = dafindex.begin(); it != dafindex.end(); it++) {
		awtindex[it->first] = idx;
		idx++;
	}
}


void Dependency::printFluentNames() const {
	cout << "STATE FLUENT NAMES" << endl;
	for (map<int, string>::const_iterator it = sfnames.begin(); it != sfnames.end(); it++) {
		cout << it->first << " " << it->second << endl;
	}
	cout << endl;

	cout << "ACTION FLUENT NAMES" << endl;
	for (map<int, string>::const_iterator it = afnames.begin(); it != afnames.end(); it++) {
		cout << it->first << " " << it->second << endl;
	}
	cout << endl;

	cout << "STATE FLUENT ARGUMENTS" << endl;
	for (int i = 0; i < nsfluents; i++) {
		cout << i << ": ";
		for (vector<string>::const_iterator it = sfargs[i].begin(); it != sfargs[i].end(); it++) {
			cout << (*it) << " ";
		}
		cout << endl;
	}
	cout << endl;


	cout << "ACTION FLUENT ARGUMENTS" << endl;
	for (int i = 0; i < nafluents; i++) {
		cout << i << ": ";
		for (vector<string>::const_iterator it = afargs[i].begin(); it != afargs[i].end(); it++) {
			cout << (*it) << " ";
		}
		cout << endl;
	}
	cout << endl;

	cout << "DISTINCT STATE FLUENT NAMES AND INDEXES" << endl;
	for (map<string, int>::const_iterator it = dsfindex.begin(); it != dsfindex.end(); it++) {
		cout << it->first << " " << it->second << endl;
	}
	cout << endl;


	cout << "DISTINCT ACTION FLUENT NAMES AND INDEXES" << endl;
	for (map<string, int>::const_iterator it = dafindex.begin(); it != dafindex.end(); it++) {
		cout << it->first << " " << it->second << endl;
	}
	cout << endl;
	//:

	cout << "DISTINCT STATE FLUENT WEIGHT INDEXES" << endl;
	for (map<string, int>::const_iterator it = swtindex.begin(); it != swtindex.end(); it++) {
		cout << it->first << " " << it->second << endl;
	}
	cout << endl;

	cout << "STATE FLUENT WEIGHT STRINGS" << endl;
	for (int i = 0; i < nsfluents; i++) {
		cout << i << ": ";
		for (vector<string>::const_iterator it = sfwts[i].begin(); it != sfwts[i].end(); it++) {
			cout << (*it) << "    ";
		}
		cout << endl;
	}
	cout << endl;

	cout << "STATE FLUENT WEIGHT INDEXES" << endl;
	for (int i = 0; i < nsfluents; i++) {
		cout << i << ": ";
		for (vector<string>::const_iterator it = sfwts[i].begin(); it != sfwts[i].end(); it++) {
			cout << swtindex.find((*it))->second << " ";
		}
		cout << endl;
	}
	cout << endl;


	cout << "DISTINCT ACTION FLUENT WEIGHT INDEXES" << endl;
	for (map<string, int>::const_iterator it = awtindex.begin(); it != awtindex.end(); it++) {
		cout << it->first << " " << it->second << endl;
	}
	cout << endl;

	cout << "ACTION FLUENT WEIGHT STRINGS" << endl;
	for (int i = 0; i < nafluents; i++) {
		cout << i << ": ";
		for (vector<string>::const_iterator it = afwts[i].begin(); it != afwts[i].end(); it++) {
			cout << (*it) << "    ";
		}
		cout << endl;
	}
	cout << endl;

	cout << "ACTION FLUENT WEIGHT INDEXES" << endl;
	for (int i = 0; i < nafluents; i++) {
		cout << i << ": ";
		for (vector<string>::const_iterator it = afwts[i].begin(); it != afwts[i].end(); it++) {
			cout << awtindex.find((*it))->second << " ";
		}
		cout << endl;
	}
	cout << endl;
}


void Dependency::printDependencies() const {
	cout << "STATE FLUENT DEPENDENCIES" << endl;
	for (int i = 0; i < nsfluents; i++) {
		cout << i << ": ";
		for (set<int>::const_iterator it = sfluents[i].begin(); it != sfluents[i].end(); it++) {
			cout << (*it) << " ";
		}
		cout << endl;
	}
	cout << endl;

	cout << "ACTION FLUENT DEPENDENCIES" << endl;
	for (int i = 0; i < nafluents; i++) {
		cout << i << ": ";
		for (set<int>::const_iterator it = afluents[i].begin(); it != afluents[i].end(); it++) {
			cout << (*it) << " ";
		}
		cout << endl;
	}
	cout << endl;
}


void Dependency::writeLinks() const {
	ofstream ofile(outfile1.c_str());
	for (int i = 0; i < nsfluents; i++) {
		stringstream ss;
		for (set<int>::const_iterator it = sfluents[i].begin(); it != sfluents[i].end(); it++) {
			ss << (*it) << ",";
		}
		string dstr = ss.str();
		dstr.pop_back();
		ofile << dstr << endl;
	}
	ofile.close();
}


// void Dependency::writeStateFluentWeights() const {
// 	ofstream ofile(outfile2.c_str());
// 	ofile << swtindex.size() << endl;
// 	for (int i = 0; i < nsfluents; i++) {
// 		stringstream ssw;
// 		for (vector<string>::const_iterator it = sfwts[i].begin(); it != sfwts[i].end(); it++) {
// 			ssw << swtindex.find((*it))->second << ",";
// 		}
// 		string wstr = ssw.str();
// 		wstr.pop_back();
// 		ofile << wstr;
// 		ofile << endl;
// 	}
// 	ofile.close();
// }
//
//
//
// void Dependency::writeActionFluentWeights() const {
// 	ofstream ofile(outfile3.c_str());
// 	ofile << awtindex.size() << endl;
// 	for (int i = 0; i < nafluents; i++) {
// 		stringstream ssw;
// 		for (vector<string>::const_iterator it = afwts[i].begin(); it != afwts[i].end(); it++) {
// 			ssw << awtindex.find((*it))->second << ",";
// 		}
// 		string wstr = ssw.str();
// 		wstr.pop_back();
// 		ofile << wstr;
// 		ofile << endl;
// 	}
// 	ofile.close();
// }


Dependency::~Dependency() {
	delete[] sfluents;
	delete[] afluents;
	delete[] sfargs;
	delete[] afargs;
	delete[] sfwts;
	delete[] afwts;
}

/*
int main(int argc, char* argv[]) {
	if ((argc < 4) || (argc > 6)) {
		cout << "usage: ./nw <infile> <links.csv> <sweights.csv> <aweights.csv>" << endl;
		exit(1);
	}

	ifstream ifile(argv[1]);
	if (!ifile) {
		cout << "Error opening input file" << endl;
		ifile.close();
		exit(1);
	}

	ofstream ofile1(argv[2]);
	if (!ofile1) {
		cout << "Error opening output file 1" << endl;
		ofile1.close();
		exit(1);
	}

	ofstream ofile2(argv[3]);
	if (!ofile2) {
		cout << "Error opening output file 2" << endl;
		ofile2.close();
		exit(1);
	}


	ofstream ofile3(argv[4]);
	if (!ofile3) {
		cout << "Error opening output file 3" << endl;
		ofile3.close();
		exit(1);
	}
	//:

	Dependency D(argv[1], argv[2], argv[3], argv[4]);
	D.process();
	D.getStateFluentWeights();
	D.getActionFluentWeights();
	D.printFluentNames();
	D.printDependencies();
	D.writeLinks();

	if (argc >= 4)
		D.writeStateFluentWeights();


	if (argc == 5)
		D.writeActionFluentWeights();
	//:

	return 0;
}
*/
