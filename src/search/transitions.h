#ifndef TRANSITIONS_H
#define TRANSITIONS_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <string>
#include <vector>
#include <set>
#include <map>

class Dependency {
private:
	std::string infile;
	std::string outfile1;
	// std::string outfile2;
	// std::string outfile3;
	int nsfluents;
	int nafluents;
	std::map<int, std::string> sfnames;
	std::map<int, std::string> afnames;
	std::map<std::string, int> dsfindex;		// distinct state fluent index
	std::map<std::string, int> dafindex;		// distinct action fluent index
	std::map<std::string, int> swtindex;		// weight index for state fluents
	std::map<std::string, int> awtindex;		// weight index for action fluents
	std::set<int>* sfluents;				// state fluents that affect a given state fluent
	std::set<int>* afluents;				// state fluents affected by an action fluent
	std::vector<std::string>* sfargs;			// state fluent arguments
	std::vector<std::string>* afargs;			// action fluent arguments
	std::vector<std::string>* sfwts;			// state fluent weight strings
	std::vector<std::string>* afwts;			// action fluent weight strings

	static const std::string sdet;
	static const std::string sprob;
	static const std::string sactions;
	static const std::string astart;
	static const std::string dstart;
	static const std::string pstart;
	static const std::string sname;
	static const std::string sindex;
	static const std::string sformula;
	static const std::string sreward;

public:
	// Dependency(const char* in, const char* out, const char* ws, const char* aws);
	Dependency(std::string in, std::string out);
	void process();
	void getStateFluentWeights();
	void getActionFluentWeights();
	void printFluentNames() const;
	void printDependencies() const;
	void writeLinks() const;
	// void writeStateFluentWeights() const;
	// void writeActionFluentWeights() const;
	~Dependency();
};

#endif
