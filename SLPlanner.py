import numpy.ctypeslib as npct
from RDDLEnv import RDDLEnv
import tensorflow as tf
import pandas as pd
import numpy as np
import argparse
import time
import pwd
import csv
import os
import psutil


class Classifier :
    def __init__(self, n, m, nw="sparse", l=[], h=0, k=1, s=True) :
        """ n: #statevars m: #actionvars a: architecture h: #hiddenlayers k: #channels l: links s:skip? """
        self.n = n
        self.m = m
        self.nw = nw
        self.l = l
        self.h = h
        self.k = k
        self.s = s


    def sparse_layer(self, x, h) :
        """ Sparsely Connected Layer """
        v = []
        b = tf.Variable(tf.truncated_normal(shape = [self.n, self.k], mean = 0, stddev = 0.1), name = "b")
        for i in range(self.n) :
            wname = "w{}".format(i)
            if h == 0 :
                wi = tf.Variable(tf.truncated_normal(shape = [len(self.l[i]), self.k], mean = 0, stddev = 0.1), name = wname)
            else :
                wi = tf.Variable(tf.truncated_normal(shape = [len(self.l[i]), self.k, self.k], mean = 0, stddev = 0.1), name = wname)
            t = []
            for j in self.l[i] :
                t.append(x[:, j])
            tt = tf.stack(t, 1)
            if h == 0 :
                qi = tf.matmul(tf.reshape(tt, [-1, len(self.l[i])]), tf.reshape(wi, [-1, self.k]))
            else :
                qi = tf.matmul(tf.reshape(tt, [-1, len(self.l[i]) * self.k]), tf.reshape(wi, [-1, self.k]))
            qi += b[i]
            v.append(qi)
        xx = tf.stack(v, 1)
        return tf.nn.relu(xx)


    def fully_connected_layer(self, x, h) :
        """ Fully Connected Layer """
        b = tf.Variable(tf.truncated_normal(shape = [self.n * self.k], mean = 0, stddev = 0.1), name = "b")
        if h == 0 :
        	w = tf.Variable(tf.truncated_normal(shape = [self.n, self.k * self.k], mean = 0, stddev = 0.1), name = "w")
        else :
        	w = tf.Variable(tf.truncated_normal(shape = [self.n * self.k, self.n * self.k], mean = 0, stddev = 0.1), name = "w")
        xx = tf.matmul(x, w) + b
        return tf.nn.relu(xx)


    def nonlinear_network(self) :
        """ Fully Connected or RDDL-based Sparsely Connected Network """
        x1 = tf.placeholder(tf.float32, [None, self.n], name="x1")
        y1 = tf.placeholder(tf.float32, [None, self.m], name="y1")
        x = x1
        for h in range(self.h) :
            hname = "h{}".format(h)
            with tf.variable_scope(hname) :
                if self.nw == "fullyconnected" :
                   x = self.fully_connected_layer(x, h)
                else :
                   x = self.sparse_layer(x, h)
        x3 = tf.reshape(x, [-1, self.k * self.n])
        r3 = tf.Variable(tf.truncated_normal(shape = [self.k * self.n, self.m], mean = 0, stddev = 0.1), name = "r3", trainable=False)
        w3 = tf.Variable(r3.initialized_value(), name = "w3")
        s3 = tf.Variable(tf.truncated_normal(shape = [self.m], mean = 0, stddev = 0.1), name = "s3", trainable = False)
        b3 = tf.Variable(s3.initialized_value(), name = "b3")
        n3 = tf.matmul(x3, w3) + b3
        # yhat = tf.nn.softmax(n3)
        yhat = tf.nn.sigmoid(n3)
        return x1, y1, yhat


    def linear_network(self) :
        """ Linear Network """
        x1 = tf.placeholder(tf.float32, [None, self.n], name="x1")
        y1 = tf.placeholder(tf.float32, [None, self.m], name="y1")
        r1 = tf.Variable(tf.truncated_normal(shape=[self.n, self.m], mean=0, stddev=0.1), name="r1", trainable=False)
        w1 = tf.Variable(r1.initialized_value(), name="w1")
        s1 = tf.Variable(tf.truncated_normal(shape=[self.m], mean=0, stddev=0.1), name="s1", trainable=False)
        b1 = tf.Variable(s1.initialized_value(), name="b1")
        n1 = tf.matmul(x1, w1) + b1
        yhat = tf.nn.softmax(n1)
        return x1, y1, yhat


    def train(self, features, labels, batchsize, lr, ckpt, trainingtime) :
        """ Train Network """
        X = np.array(features, dtype = np.float)
        Y = np.array(labels, dtype = np.float)
        if self.nw == "linear" :
            X1, Y1, YHAT = self.linear_network()
        else :
            X1, Y1, YHAT = self.nonlinear_network()
        # LOSS = - tf.reduce_sum(Y1 * tf.log(tf.clip_by_value(YHAT, 1e-10, 1.0)))
        LOSS = - tf.reduce_sum((Y1 * tf.log(tf.clip_by_value(YHAT, 1e-10, 1.0))) + ((1.0 - Y1) * tf.log(tf.clip_by_value((1.0 - YHAT), 1e-10, 1.0))))
        tf.identity(LOSS, "LOSS")
        optimizer = tf.train.AdamOptimizer(lr)
        TSTEP = optimizer.minimize(LOSS)

        saver = tf.train.Saver()
        conf = tf.ConfigProto(intra_op_parallelism_threads = 2, inter_op_parallelism_threads = 3)
        sess = tf.Session(config = conf)
        sess.run(tf.global_variables_initializer());
        tf.add_to_collection("X1", X1)
        tf.add_to_collection("Y1", Y1)
        tf.add_to_collection("YHAT", YHAT)
        tf.add_to_collection("LOSS", LOSS)

        num_batches = ((X.shape[0] - 1) // batchsize) + 1
        memory_limit = 3584000000 #3.5 GB
        process = psutil.Process(os.getpid())
        epoch = 0; st = time.time(); time_exceeded = False; memory_exceeded = False
        while not time_exceeded and not memory_exceeded:
            Z = np.concatenate((X, Y), axis=1)
            np.random.shuffle(Z)
            X, Y = Z[:,0:self.n], Z[:,self.n:self.n+self.m]
            epochloss = 0
            for j in range(num_batches) :
                start = j * batchsize
                XB = X[start:start+batchsize]
                YB = Y[start:start+batchsize]
                yhat, _, loss = sess.run([YHAT, TSTEP, LOSS], feed_dict={X1: XB, Y1: YB})
                epochloss += loss
                memory_usage = process.memory_info().rss
                if memory_usage > memory_limit:
                    print('Memory limit exceeded')
                    memory_exceeded = True
                    break
                if (time.time() - st) > trainingtime :
                    print("Time limit reached for training")
                    time_exceeded = True
                    break
            epoch += 1
            print('epoch: {}  loss: {}'.format(epoch, epochloss))

        saver.save(sess, ckpt)
        sess.close()


global sess1, l, homedir, trainingtime

def cbtest(state, probs):
    """ Test Callback """
    global sess1
    s = npct.as_array(state, (env.num_state_vars,))
    a = np.zeros(env.num_action_vars, dtype = np.int)
    with sess1.as_default() as sess :
        X1 = tf.get_collection("X1")[0]
        Y1 = tf.get_collection("Y1")[0]
        YHAT = tf.get_collection("YHAT")[0]
        LOSS = tf.get_collection("LOSS")[0]
        yhat = sess.run(YHAT, feed_dict={X1: np.matrix(s, np.float), Y1: np.matrix(a, np.int)})
        for i in range(env.num_action_vars):
            probs[i] = yhat[0][i]


def cbinit() :
    global l, homedir
    l = []
    homedir = pwd.getpwuid(os.getuid()).pw_dir
    if args.network == "sparse" :
        lfile = os.path.join(homedir, args.env + '_links.csv')
        with open(lfile, newline="") as cfile :
            creader = csv.reader(cfile, quoting = csv.QUOTE_NONNUMERIC)
            for row in creader :
                l.append([int(x) for x in row])
        print(l)
    return cbtime()


def cbtime() :
    global l, trainingtime
    stime = time.time()
    T = Classifier(env.num_state_vars, env.num_action_vars, args.network, l, args.hiddenlayers, args.channels)
    with tf.Graph().as_default() :
        if args.network == "linear" :
            X1, Y1, YHAT = T.linear_network()
        else :
            X1, Y1, YHAT = T.nonlinear_network()
        sess = tf.Session()
        sess.run(tf.global_variables_initializer());
        NUM_EPISODES = int(env.num_rounds / 5)
        for i in range(NUM_EPISODES):
            reward = 0
            rwd = 0
            curr = env.reset()
            done = False
            while not done :
                aindex = []
                actionarray = np.zeros(env.num_action_vars, dtype = np.int8)
                yhat = sess.run(YHAT, feed_dict={X1: np.matrix(curr, np.float), Y1: np.matrix(actionarray, np.int8)})
                actionarray = np.random.randint(2, size=env.num_action_vars)
                nxt, rwd, done, _ = env.doAction(actionarray)
                #print('state: {}  action: {}  reward: {} next: {}'.format(curr, actionarray, rwd, nxt))
                curr = nxt
                reward += rwd
        sess.close()
    etime = time.time()
    t1 = (etime - stime) * int(env.num_rounds * 2)
    t2 = (((env.remaining_time / 1000) - t1)) * 0.8
    rollouttime = t2 * 0.3
    trainingtime = t2 * 0.5
    print('Rollout: {}  Training: {}  Evaluation: {} '.format(rollouttime, trainingtime, t1))
    return rollouttime


def cbtrain() :
    """ Train Callback """
    global l, homedir, trainingtime
    dataset = os.path.join(homedir, args.env + '_training.csv')
    ckpt = os.path.join(homedir, args.env + '_model.ckpt')
    A1 = pd.read_csv(dataset, header = None)
    B1 = A1.as_matrix()
    F1 = B1[:, 0:env.num_state_vars]
    L1 = B1[:, env.num_state_vars:env.num_state_vars+env.num_action_vars]

    C = Classifier(env.num_state_vars, env.num_action_vars, args.network, l, args.hiddenlayers, args.channels)
    with tf.Graph().as_default() :
    	C.train(F1, L1, args.batchsize, args.lr, ckpt, trainingtime)
    meta = ckpt + ".meta"
    global sess1
    sess1 = tf.Session()
    model = tf.train.import_meta_graph(meta)
    model.restore(sess1, ckpt)


if __name__ == "__main__" :
    parser = argparse.ArgumentParser(description='SL-Planner')
    parser.add_argument('--network', default='sparse', help='Network type: linear, fullyconnected, sparse')
    parser.add_argument('--hiddenlayers', type=int, default=3, help='Number of hidden layers')
    parser.add_argument('--channels', type=int, default=5, help='Number of hidden layers')
    parser.add_argument('--batchsize', type=int, default=10, help='Batch size for training')
    parser.add_argument('--lr', type=float, default=0.0005, help='Learning rate for Adam')
    parser.add_argument('--env', default='sysadmin_inst_mdp__1', metavar='ENV', help='Problem instance')
    parser.add_argument('--host', default='10.0.2.2', help='RDDLSim server host name')
    parser.add_argument('--port', default=2323, help='RDDLSim server port')
    args = parser.parse_args()
    env = RDDLEnv(args.env)
    env.connectToServer(args.host, args.port, cbtrain, cbtest, cbinit)
